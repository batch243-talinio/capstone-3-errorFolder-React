const express = require("express");
const router = express.Router();
const auth = require("../auth")

const prodController = require("../controllers/productControllers")

// Add product (admin)
	router.post("/createProduct", auth.verify, prodController.checkProductExists, prodController.createProduct);

// Get all active product/s (all client - logged in or not logged in)
	router.get("/activeProducts", prodController.getActiveProducts);

// Get all products (admin)
	router.get("/allProducts", auth.verify, prodController.getAllProducts);

// Get one product (all client - logged in or not logged in)
	router.get("/getOneProduct/:productId", prodController.getOneProduct);

// Update specific product (admin)
	router.put("/update/:productId", auth.verify, prodController.updateProduct);

// Archive and un-Archive a product (admin)
	router.patch("/archive/:productId", auth.verify, prodController.archiveAndUnarchive);


module.exports = router;