const ATCart = require("../models/AddToCart");
const Product = require("../models/Product");
const User = require("../models/User");

const bcrypt = require("bcrypt");
const auth = require("../auth");

// Checking if Email from the user is already taken or not.

module.exports.checkEmailExists = async (request, response, next) => {
	const result = await User.find({ email: request.body.email });
	let message = ``;
	if (result.length > 0) {
		//message = `The mail ${request.email} is already taken. Please use other email.`;
		return response.send({ emailExists: true });

	} else {
		next();
	}
}

// Register the User
module.exports.registerUser = (request, response) =>{

	let newUser = new User({
		email: 		request.body.email,
		mobileNo: request.body.mobileNo,
		password: 	bcrypt.hashSync(request.body.password1, 10)
	
	})

	if(newUser.mobileNo >= 11){
		return newUser.save().then(user => {
			response.json(user)
		}).catch(error => response.send({registered: false}))
	}else{
        response.send({registered: false, mobileNoLength: false})
    }
}


// Log-in User with Authentication
module.exports.loginUser = async (request, response) => {
	
	const result = await User.findOne({ email: request.body.email });
	if (result == null) {
		response.json(false);
	} else {
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

		if (isPasswordCorrect) {
			return User.findOne({ email: request.body.email })
				.then(result_1 => {
					response.json(`${auth.createAccessToken(result_1)}`);
				});
		} else {
			response.json(false);
		}
	}
}

module.exports.getProfile = async (request, response) =>{

	try {
		const result = await User.findById(request.body.id);
		result.password = "******";
		return response.send(result);
	} catch (error) {
		return response.send(error);
	}
}

module.exports.profileDetails = async (request, response) =>{
	// user will be object that contains the id and email of the user that is currently logged in.
	const userData = auth.decode(request.headers.authorization);

	try {
		const result = await User.findById(userData.id);
		result.password = "Confindential";
		return response.send(result);
	} catch (err) {
		return response.send(err);
	}
}	

// Update role (admin to not-admin, vice-versa)
module.exports.updateUserRole = (request, response) => {

	let userData = auth.decode(request.headers.authorization);

	let idToBeUpdated = request.params.userId;

	if(userData.isAdmin){
		return User.findById(idToBeUpdated).then(result => {

			let update = {
				isAdmin : !result.isAdmin
			}

			return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
					result.password = "Confidential."
					response.send(`User role updated! \n ${document}`);

				}).catch(err => response.send(err))
		
		}).catch(err => response.send(err));

	}else{
		return response.send("You don't have access to this page!")
	}
}


// Check order if it's more than the stock available 
module.exports.productQuantity = (request, response, next) => {
    let userData = auth.decode(request.headers.authorization);
    let productId = request.params.productId;

    Product.findById(productId).then(result => { 
        if (result.quantity < request.body.products.quantity){
            return response.send(`You ordered more than the current available stock. Please try again.`)
        }
        next();
    }).catch(error => `error here ${false}`)
}


// Check-out / Buy Product (User only)
module.exports.checkOutProduct = async (request, response, next) => {
	let userData = auth.decode(request.headers.authorization);
	let productId = request.params.productId;

	if(userData.isAdmin){ return response.send("You don't have access to this page!")}

	// Inserting userInfo to PRODUCTS
	let isProductUpdated = await Product.findById(productId).then(async result => {
		result.orders.push({
			orderId: userData.id
		})
		result.quantity -= request.body.products.quantity

		try {
			const success = await result.save();
			return true;
		} catch (error) {
			return false;
		}
	}).catch(error => response.send(false))

	let prodName; let prodPrice;
	let details = await Product.findById(productId).then(result => { prodName = result.name; prodPrice = result.price; }).catch(error => `error here ${false}`)

	// Inserting checked-out product/s to USER
	let isUserUpdated = await User.findById(userData.id).then(async result =>{
		result.credit = result.credit - (prodPrice * request.body.products.quantity)
		result.orders.push({
			products: {
				productName: prodName,
            	quantity: request.body.products.quantity
			},
			totalAmount: prodPrice * request.body.products.quantity
		})

		try {
			const success = await result.save();
			return true;
		} catch (error) {
			return false;
		}
	}).catch(error => response.send(false))


	// Check if all changes are made or successful
	return (isProductUpdated && isUserUpdated) ? next() : response.send(`An error occured. Please try again.`)
}

module.exports.baughtProduct = (request, response) => {
	return response.send(`!!Transaction Complete!! \nProduct checked-out successfully. Please come again.`);
}

// Remove added to cart products
module.exports.removeCart = (request, response) => {
    let userData = auth.decode(request.headers.authorization);
    let productIdz = request.params.productId;
    
    ATCart.findOne({userId: userData.id, productId: productIdz}).then(async result => {
    	const cartId = result.id;
    	let update = {
				isActive : false
			}

		try {
			const document = await ATCart.findByIdAndUpdate(cartId, update, { new: true });
			response.send(`!!Transaction Complete!! \nProduct checked-out successfully. Please come again.`);
		} catch (err) {
			return response.send(err);
		}
    }).catch(error => `error here ${false}`)
    

        
}

/*
module.exports.checkEmailExists = (request, response, next) => {
	return User.find({email: request.body.email}).then(result => {
		let message = ``;
		if(result.length > 0){
			//message = `The mail ${request.email} is already taken. Please use other email.`;
			return response.send({emailExists: true});

		}else{
			next();
		}
	})
}

// Register the User
module.exports.registerUser = (request, response) =>{

	let newUser = new User({
		email: 		request.body.email,
		mobileNo: request.body.mobileNo,
		password: 	bcrypt.hashSync(request.body.password1, 10)
	
	})

	if(newUser.mobileNo >= 11){
		return newUser.save().then(user => {
			response.json(user)
		}).catch(error => response.send({registered: false}))
	}else{
        response.send({registered: false, mobileNoLength: false})
    }
}


// Log-in User with Authentication
module.exports.loginUser = (request, response) => {
	
	return User.findOne({email : request.body.email})
	.then(result => {

		if(result == null){
			response.send('')
		}else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){
				return User.findOne({email : request.body.email})
					.then(result => {
						//console.log(result)
						response.json(`${auth.createAccessToken(result)}`)
					})

			}else{
				return response.send(false)
			}
		}
	})
}

module.exports.getProfile = (request, response) =>{

	return User.findById(request.body.id).then(result => {
		result.password = "******";
		//console.log(result);
		return response.send(result);
	}).catch(error => {
		//console.log(error);
		return response.send(error);
	})
}

module.exports.profileDetails = (request, response) =>{
	// user will be object that contains the id and email of the user that is currently logged in.
	const userData = auth.decode(request.headers.authorization);

	return User.findById(userData.id)
	.then(result => {
		result.password = "Confindential";
		return response.send(result)
	}).catch(err => {
		return response.send(err);
	})
}	

// Update role (admin to not-admin, vice-versa)
module.exports.updateUserRole = (request, response) => {

	let userData = auth.decode(request.headers.authorization);

	let idToBeUpdated = request.params.userId;

	if(userData.isAdmin){
		return User.findById(idToBeUpdated).then(result => {

			let update = {
				isAdmin : !result.isAdmin
			}

			return User.findByIdAndUpdate(idToBeUpdated, update, {new: true}).then(document => {
					result.password = "Confidential."
					response.send(`User role updated! \n ${document}`);

				}).catch(err => response.send(err))
		
		}).catch(err => response.send(err));

	}else{
		return response.send("You don't have access to this page!")
	}
}


// Check order if it's more than the stock available 
module.exports.productQuantity = (request, response, next) => {
    let userData = auth.decode(request.headers.authorization);
    let productId = request.params.productId;

    Product.findById(productId).then(result => { 
        if (result.quantity < request.body.products.quantity){
            return response.send(`You ordered more than the current available stock. Please try again.`)
        }
        next();
    }).catch(error => `error here ${false}`)
}


// Check-out / Buy Product (User only)
module.exports.checkOutProduct = async (request, response, next) => {
	let userData = auth.decode(request.headers.authorization);
	let productId = request.params.productId;

	if(userData.isAdmin){ return response.send("You don't have access to this page!")}

	// Inserting userInfo to PRODUCTS
	let isProductUpdated = await Product.findById(productId).then(result => {
		result.orders.push({
			orderId: userData.id
		})
		result.quantity -= request.body.products.quantity

		return result.save().then(success => true ).catch(error => false)
	}).catch(error => response.send(false))

	let prodName; let prodPrice;
	let details = await Product.findById(productId).then(result => { prodName = result.name; prodPrice = result.price; }).catch(error => `error here ${false}`)

	// Inserting checked-out product/s to USER
	let isUserUpdated = await User.findById(userData.id).then(result =>{
		result.credit = result.credit - (prodPrice * request.body.products.quantity)
		result.orders.push({
			products: {
				productName: prodName,
            	quantity: request.body.products.quantity
			},
			totalAmount: prodPrice * request.body.products.quantity
		})

		return result.save().then(success => true ).catch(error =>  false)
	}).catch(error => response.send(false))


	// Check if all changes are made or successful
	return (isProductUpdated && isUserUpdated) ? next() : response.send(`An error occured. Please try again.`)
}

module.exports.baughtProduct = (request, response) => {
	return response.send(`!!Transaction Complete!! \nProduct checked-out successfully. Please come again.`);
}

// Remove added to cart products
module.exports.removeCart = (request, response) => {
    let userData = auth.decode(request.headers.authorization);
    let productIdz = request.params.productId;
    
    ATCart.findOne({userId: userData.id, productId: productIdz}).then(result => {
    	const cartId = result.id;
    	let update = {
				isActive : false
			}

		return ATCart.findByIdAndUpdate(cartId, update, {new: true}).then(document => {
			response.send(`!!Transaction Complete!! \nProduct checked-out successfully. Please come again.`);

		}).catch(err => response.send(err))
    }).catch(error => `error here ${false}`)
    

        
}
*/